import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


@WebServlet("/DemoServlet")
public class DemoServlet extends HttpServlet {
    private String name = "";
    private String lastname = "";

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<h2>Name and Lastname in DB</h2>");
        out.println("<h3>Name: " + name + "</h3>");
        out.println("<h3>Lastname: " + lastname + "</h3>");
        out.println("<h2>Name and Lastname sent by GET</h2>");
        request.getRequestDispatcher("/get.jsp").include(request, response);
    }
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        if(request.getParameter("name") != null)
        {
            name = request.getParameter("name");
        }

        if(request.getParameter("lastname") != null)
        {
            lastname = request.getParameter("lastname");
        }
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        name = request.getParameter("name");
        lastname = request.getParameter("lastname");
        response.setContentType("text/html");
        request.getRequestDispatcher("/post.jsp").include(request, response);
    }
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        name = "";
        lastname = "";
        response.setContentType("text/html");
    }
}
